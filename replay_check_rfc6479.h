#pragma once

#include <stdint.h>

#define REPLAY_CHECK_BITMAP_BLOCKS 32U /* 32 32-bit blocks for (up to) 992-elements
                                        * replay check window. You might want to consult
                                        * rfc6479 if you decide to change this constant. */
#ifdef __cplusplus
extern "C" {
#endif

struct replay_check_context {
  uint16_t window;
  uint16_t last_sn;
  uint32_t bitmap[REPLAY_CHECK_BITMAP_BLOCKS];
};

int replay_check(struct replay_check_context *ctx, uint16_t sn);
int replay_update(struct replay_check_context *ctx, uint16_t sn);
int replay_check_update(struct replay_check_context *ctx, uint16_t sn);
int compute_block_distance_a_d(int a, int d, int block_size); /*exported for test*/

#ifdef __cplusplus
}
#endif
