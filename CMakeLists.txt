CMAKE_MINIMUM_REQUIRED(VERSION 3.15)
PROJECT(sn_replay)
SET(CMAKE_CXX_STANDARD 11)
FIND_PACKAGE(Threads REQUIRED)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra -pedantic")

ADD_LIBRARY(replay_check
  replay_check_rfc6479.h
  replay_check_rfc6479.c)

ADD_EXECUTABLE(sn_replay main.cpp)

TARGET_LINK_LIBRARIES(sn_replay replay_check gtest gtest_main ${CMAKE_THREAD_LIBS_INIT})
