#include <string.h>
#include "replay_check_rfc6479.h"

/* https://stackoverflow.com/questions/22075058/bit-position-of-a-number-at-compile-time*/
/* https://stackoverflow.com/questions/3957252/is-there-any-way-to-co
 * mpute-the-width-of-an-integer-type-at-compile-time/4589384#4589384 */
#define REPLAY_CHECK_IMAX_BITS(m) ((m) / ((m)%0x3fffffffL+1) / 0x3fffffffL %0x3fffffffL *30 \
                                 + (m)%0x3fffffffL / ((m)%31+1) / 31%31*5 + 4-12 / ((m)%31+3))

#define REPLAY_CHECK_BITS_PER_BLOCK     (sizeof *((struct replay_check_context*)0)->bitmap << 3)
#define REPLAY_CHECK_BIT_LOCATION_MASK  (REPLAY_CHECK_BITS_PER_BLOCK-1)
#define REPLAY_CHECK_BIT_LOCATION_WIDTH (REPLAY_CHECK_IMAX_BITS(REPLAY_CHECK_BIT_LOCATION_MASK))
#define REPLAY_CHECK_BLOCK_INDEX_MASK   (REPLAY_CHECK_BITMAP_BLOCKS-1)

static int16_t sn_diff(uint16_t a, uint16_t b) {
  return (int16_t)(b - a);
}

int compute_block_distance_a_d(int a, int d, int block_size) {
  int b = a%block_size + d;
  return b/block_size;
}

int replay_check(struct replay_check_context *ctx, uint16_t sn) {
  int16_t const sn_distance = sn_diff(ctx->last_sn, sn);
  if (sn_distance > 0) {
    /*ahead of the window*/
    return 1;
  }
  else if (sn_distance < -(int16_t)ctx->window) {
    /*behind the window*/
    return 0;
  }

  int const bit_location = sn & REPLAY_CHECK_BIT_LOCATION_MASK;
  int const block_index = (sn >> REPLAY_CHECK_BIT_LOCATION_WIDTH) & REPLAY_CHECK_BLOCK_INDEX_MASK;

  if (ctx->bitmap[block_index] & (1U << bit_location)) {
    return 0;
  }

  return 1;
}

int replay_update(struct replay_check_context *ctx, uint16_t sn) {
  int16_t const sn_distance = sn_diff(ctx->last_sn, sn);
  if (sn_distance < -(int16_t)ctx->window) {
    /*behind the window*/
    return 0;
  }

  int const bit_location = sn & REPLAY_CHECK_BIT_LOCATION_MASK;
  int const block_index = (sn >> REPLAY_CHECK_BIT_LOCATION_WIDTH) & REPLAY_CHECK_BLOCK_INDEX_MASK;

  if (sn_distance <= 0) {
    /*within the window*/
    if (ctx->bitmap[block_index] & (1U << bit_location)) {
      return 0;
    }
    ctx->bitmap[block_index] |= (1U << bit_location);
    return 1;
  }

  /*ahead of the window*/

  int const block_distance = compute_block_distance_a_d(ctx->last_sn, sn_distance, REPLAY_CHECK_BITS_PER_BLOCK);
  if (block_distance >= REPLAY_CHECK_BITMAP_BLOCKS) {
    memset(ctx->bitmap, 0, sizeof ctx->bitmap);
  }
  else {
    int const prev_block_index = (ctx->last_sn >> REPLAY_CHECK_BIT_LOCATION_WIDTH) & REPLAY_CHECK_BLOCK_INDEX_MASK;
    for (unsigned n = 1; n <= block_distance; ++n) {
      ctx->bitmap[(prev_block_index+n)%REPLAY_CHECK_BITMAP_BLOCKS] = 0;
    }
  }

  ctx->last_sn = sn;
  ctx->bitmap[block_index] |= (1U << bit_location);

  return 1;
}

int replay_check_update(struct replay_check_context *ctx, uint16_t sn) {
  int rc = replay_check(ctx, sn);
  if (rc) {
    rc = replay_update(ctx, sn);
  }
  return rc;
}
