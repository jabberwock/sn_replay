#include <gtest/gtest.h>
#include "replay_check_rfc6479.h"

TEST(replay, block_distance) {

  EXPECT_EQ(0, compute_block_distance_a_d(0, 0, 8));
  EXPECT_EQ(0, compute_block_distance_a_d(0, 7, 8));
  EXPECT_EQ(0, compute_block_distance_a_d(1, 0, 8));
  EXPECT_EQ(0, compute_block_distance_a_d(31, 0, 8));
  EXPECT_EQ(0, compute_block_distance_a_d(0, 1, 8));
  EXPECT_EQ(0, compute_block_distance_a_d(30, 1, 8));
  EXPECT_EQ(1, compute_block_distance_a_d(31, 1, 8));
  EXPECT_EQ(4, compute_block_distance_a_d(0, 32, 8));
  EXPECT_EQ(7, compute_block_distance_a_d(0, 63, 8));
  EXPECT_EQ(8, compute_block_distance_a_d(0, 64, 8));
  EXPECT_EQ(2, compute_block_distance_a_d(11, 20, 8));
}

TEST(replay, 1) {
  replay_check_context ctx = {8, 0, {0}};
  EXPECT_TRUE(replay_check(&ctx, 0));
  EXPECT_TRUE(replay_check(&ctx, 1));
  EXPECT_TRUE(replay_check(&ctx, 2));
  EXPECT_TRUE(replay_check(&ctx, 3));
  EXPECT_TRUE(replay_check(&ctx, 4));
  EXPECT_TRUE(replay_check(&ctx, 5));
  EXPECT_TRUE(replay_check(&ctx, 6));
  EXPECT_TRUE(replay_check(&ctx, 7));
  EXPECT_TRUE(replay_check(&ctx, 8));
  EXPECT_TRUE(replay_check(&ctx, 9));
  EXPECT_TRUE(replay_check(&ctx, 10));
  EXPECT_TRUE(replay_check(&ctx, 11));
  EXPECT_TRUE(replay_check(&ctx, 12));
  EXPECT_TRUE(replay_check(&ctx, 13));
  EXPECT_TRUE(replay_check(&ctx, 14));
  EXPECT_TRUE(replay_check(&ctx, 15));
  EXPECT_TRUE(replay_check(&ctx, 16));
  EXPECT_TRUE(replay_check(&ctx, 17));
  EXPECT_TRUE(replay_check(&ctx, 18));
  EXPECT_TRUE(replay_check(&ctx, 19)); /*probed up to 0..19 above the WT*/

  EXPECT_TRUE(replay_check_update(&ctx, 20)); /*updated WT to 20*/

  EXPECT_FALSE(replay_check(&ctx, 1));
  EXPECT_FALSE(replay_check(&ctx, 0));
  EXPECT_FALSE(replay_check(&ctx, 2));
  EXPECT_FALSE(replay_check(&ctx, 3));
  EXPECT_FALSE(replay_check(&ctx, 4));
  EXPECT_FALSE(replay_check(&ctx, 5));
  EXPECT_FALSE(replay_check(&ctx, 6));
  EXPECT_FALSE(replay_check(&ctx, 7));
  EXPECT_FALSE(replay_check(&ctx, 8));
  EXPECT_FALSE(replay_check(&ctx, 9));
  EXPECT_FALSE(replay_check(&ctx, 10));
  EXPECT_FALSE(replay_check(&ctx, 11)); /*probed exceeding window depth (negative result)*/

  EXPECT_TRUE(replay_check(&ctx, 12));
  EXPECT_TRUE(replay_check(&ctx, 13));
  EXPECT_TRUE(replay_check(&ctx, 14));
  EXPECT_TRUE(replay_check(&ctx, 15));
  EXPECT_TRUE(replay_check(&ctx, 16));
  EXPECT_TRUE(replay_check(&ctx, 17));
  EXPECT_TRUE(replay_check(&ctx, 18));
  EXPECT_TRUE(replay_check(&ctx, 19)); /*probed within the window (positive result)*/

  EXPECT_FALSE(replay_check(&ctx, 20)); /*probed known element (negative result)*/

  EXPECT_TRUE(replay_check(&ctx, 21));
  EXPECT_TRUE(replay_check(&ctx, 22));
  EXPECT_TRUE(replay_check(&ctx, 23));
  EXPECT_TRUE(replay_check(&ctx, 24));
  EXPECT_TRUE(replay_check(&ctx, 25)); /*probed up to somewhere above the WT*/
}

TEST(replay, 2) {
  replay_check_context ctx = {8, 0, {0}};

  EXPECT_TRUE(replay_check_update(&ctx, 0));
  EXPECT_TRUE(replay_check_update(&ctx, 1));
  EXPECT_TRUE(replay_check_update(&ctx, 2));
  EXPECT_TRUE(replay_check_update(&ctx, 3));
  EXPECT_TRUE(replay_check_update(&ctx, 4));
  EXPECT_TRUE(replay_check_update(&ctx, 5)); /*updated 1..6 bits in block 0*/

  EXPECT_TRUE(replay_check_update(&ctx, 32767+5)); /*updated window sn forward*/
  EXPECT_TRUE(replay_check_update(&ctx, 65535));   /*updated window sn up to wraparound point*/

  EXPECT_FALSE(replay_check(&ctx, 65535 - 15));
  EXPECT_FALSE(replay_check(&ctx, 65535 - 14));
  EXPECT_FALSE(replay_check(&ctx, 65535 - 13));
  EXPECT_FALSE(replay_check(&ctx, 65535 - 12));
  EXPECT_FALSE(replay_check(&ctx, 65535 - 11));
  EXPECT_FALSE(replay_check(&ctx, 65535 - 10));
  EXPECT_FALSE(replay_check(&ctx, 65535 - 9)); /*probed exceeding window depth (negative result)*/

  EXPECT_TRUE(replay_check(&ctx, 65535 - 8));
  EXPECT_TRUE(replay_check(&ctx, 65535 - 7));
  EXPECT_TRUE(replay_check(&ctx, 65535 - 6));
  EXPECT_TRUE(replay_check(&ctx, 65535 - 5));
  EXPECT_TRUE(replay_check(&ctx, 65535 - 4));
  EXPECT_TRUE(replay_check(&ctx, 65535 - 3));
  EXPECT_TRUE(replay_check(&ctx, 65535 - 2));
  EXPECT_TRUE(replay_check(&ctx, 65535 - 1)); /*probed within the window (positive result)*/

  EXPECT_FALSE(replay_check(&ctx, 65535 - 0)); /*probed known element (negative result)*/

  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 1)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 2)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 3)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 4)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 5)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 6)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 7)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 8)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 9)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 10))); /*probed up to somewhere above the WT*/

  EXPECT_TRUE(replay_check_update(&ctx, (uint16_t)(65535+1))); /*updated window sn causing wraparound*/

  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 2)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 3)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 4)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 5)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 6)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 7)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 8)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 9)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 10)));
  EXPECT_TRUE(replay_check(&ctx, (uint16_t) (65535 + 11))); /*probed up to somewhere above the WT*/
}

TEST(replay, zeroing_blocks) {

  replay_check_context ctx = {8, 0, {0}};

  EXPECT_EQ(0, ctx.bitmap[0]);
  EXPECT_EQ(0, ctx.bitmap[1]);
  EXPECT_EQ(0, ctx.bitmap[2]);

  for (unsigned n = 1; n < 32; ++n) {
    ctx.bitmap[n] = 0xFFFFFFFF;
  }

  EXPECT_EQ(         0, ctx.bitmap[0]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[1]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[2]);

  EXPECT_TRUE(replay_check_update(&ctx, 0));
  EXPECT_EQ(0x00000001, ctx.bitmap[0]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[1]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[2]);

  EXPECT_TRUE(replay_check_update(&ctx, 1));
  EXPECT_EQ(0x00000003, ctx.bitmap[0]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[1]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[2]);

  EXPECT_TRUE(replay_check_update(&ctx, 32));
  EXPECT_EQ(0x00000003, ctx.bitmap[0]);
  EXPECT_EQ(0x00000001, ctx.bitmap[1]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[2]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[3]);

  EXPECT_TRUE(replay_check_update(&ctx, 64));
  EXPECT_TRUE(replay_check_update(&ctx, 65));
  EXPECT_EQ(0x00000003, ctx.bitmap[0]);
  EXPECT_EQ(0x00000001, ctx.bitmap[1]);
  EXPECT_EQ(0x00000003, ctx.bitmap[2]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[3]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[4]);

  EXPECT_TRUE(replay_check_update(&ctx, 128));
  EXPECT_EQ(0x00000003, ctx.bitmap[0]);
  EXPECT_EQ(0x00000001, ctx.bitmap[1]);
  EXPECT_EQ(0x00000003, ctx.bitmap[2]);
  EXPECT_EQ(0x00000000, ctx.bitmap[3]);
  EXPECT_EQ(0x00000001, ctx.bitmap[4]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[5]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[6]);

  EXPECT_TRUE(replay_check_update(&ctx, 256));
  EXPECT_EQ(0x00000003, ctx.bitmap[0]);
  EXPECT_EQ(0x00000001, ctx.bitmap[1]);
  EXPECT_EQ(0x00000003, ctx.bitmap[2]);
  EXPECT_EQ(0x00000000, ctx.bitmap[3]);
  EXPECT_EQ(0x00000001, ctx.bitmap[4]);
  EXPECT_EQ(0x00000000, ctx.bitmap[5]);
  EXPECT_EQ(0x00000000, ctx.bitmap[6]);
  EXPECT_EQ(0x00000000, ctx.bitmap[7]);
  EXPECT_EQ(0x00000001, ctx.bitmap[8]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[9]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[10]);

  memset(ctx.bitmap, 0xFF, sizeof ctx.bitmap);
  ctx.window = 8;
  ctx.last_sn = 65504;
  ctx.bitmap[31] =0x1;
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[29]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[30]);
  EXPECT_EQ(0x00000001, ctx.bitmap[31]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[0]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[1]);

  EXPECT_TRUE(replay_check_update(&ctx, 0));
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[29]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[30]);
  EXPECT_EQ(0x00000001, ctx.bitmap[31]);
  EXPECT_EQ(0x00000001, ctx.bitmap[0]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[1]);
  EXPECT_EQ(0xFFFFFFFF, ctx.bitmap[2]);
}
